# Semana3_Rebeca

***



# Introduction

This is a basic JAVA companion app (*CLI*) for Software Development 3, allowing the  **CRUD** of users. Created following the **Layer Arquitecture**. 

In summary this is a console application, that works with a Relational DB (Mysql) using the port 3306.

***

# C4 (Component)



***

# DB Schema

We use the following DB Schema (MySQL, we use schema **sd3** for this example):

```
CREATE TABLE `sd3`.`users` (
  `id` VARCHAR(36) NOT NULL,
  `name` VARCHAR(200) NOT NULL,
  `login` VARCHAR(20) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE);
```

# Usage

Just compile and run this program:

```
Users CLI

Usage: users -config=<configuration> [COMMAND]
CRUD on a Users DB
      -config=<configuration>
         Configuration File (xml)
Commands:
  -read    Read Users
  -delete  Delete a User by ID
  -create  Create a new user
  -update  Update an existing user
```

These are common parameters:

_In this examples, sd3.xml is a file that follows the **Db Configuration File**_

* Read users:

```
-config=../User_DB/sd3.xml -read
```

* Read user using pagination (5 by each):

```
-config=../User_DB/sd3.xml -readByPage -pg 1
```

* Create user 
```
-config=../User_DB/sd3.xml -create -n spiderwick -l th -p pass2003
```

* Delete existing user (id = aab5d5fd-70c1-11e5-a4fb-b026b977eb28 )
```
-config=../User_DB/sd3.xml -delete aab5d5fd-70c1-11e5-a4fb-b026b977eb28 
```

* Update existing user (id = 3bf71036-e7ef-4890-b79b-91496c14160f)
```
-config=../User_DB/sd3.xml -update -i 3bf71036-e7ef-4890-b79b-91496c14160f -n javier2 -l jroca2 -p pwd321
```



# Prove of functionality implemented



