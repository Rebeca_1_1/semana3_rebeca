package org.jala.university;

import picocli.CommandLine;

public class Main {

  public static void main(String[] args) {
    System.out.println("Users CLI");
    CommandLine cmd = new CommandLine(new UsersCommand());
    cmd.setExecutionStrategy(new CommandLine.RunAll()).execute(args);
  }
}