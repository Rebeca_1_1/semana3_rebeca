package org.example;

import java.util.List;
import org.jala.university.User;

public interface UsersService {
    int deleteUser(String id) throws Exception;
    List<User> getUsers()  throws Exception;
    void createUser(User user)  throws Exception;
    void updateUser(User user)  throws Exception;
    List<User> getUserByPage(int begin, int end) throws Exception;
}
