package org.jala.university;


import org.example.UsersService;

public class Command {
    protected UsersService getUsersService() throws Exception {
        return ServicesFacade.getInstance().getUsersService();
    }
}
