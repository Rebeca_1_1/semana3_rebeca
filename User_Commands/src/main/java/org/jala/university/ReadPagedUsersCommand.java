package org.jala.university;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import picocli.CommandLine;

@CommandLine.Command(name = "-readByPage", description = "Read Users")
public class ReadPagedUsersCommand extends Command implements Callable<Integer> {

  @CommandLine.Option(description = "Pagination 5 users each", required = true, names = {"-pg"})
  protected int page;


  @Override
  public Integer call() throws Exception {
    try {
      int endUsersToShow = page * 5;
      int beginUserToShow = endUsersToShow - 5;
      var users = getUsersService().getUserByPage(beginUserToShow, endUsersToShow);
      System.out.printf("Users found: [%d]\n", users.size());
      AtomicInteger count = new AtomicInteger(0);
      for (User user: users) {
        System.out.printf("[%d] %s\n", count.addAndGet(1), user);
      }
      return 0;
    }
    catch (Exception ex) {
      System.out.printf("Cannot read users. %s", ex.getMessage());
      throw ex;
    }
  }
}